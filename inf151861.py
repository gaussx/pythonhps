import numpy as np 
from pylab import *
from scipy.io import wavfile
import os
import matplotlib.pyplot as plt

MALE_FREQ = [80, 160]
FEMALE_FREQ = [161, 255]

WAV_DIR_PATH = "train/"

HPS_TIMES = 5
correct_males, incorrect_males, correct_females, incorrect_females = 0, 0, 0, 0

if __name__ == "__main__":
    files = [WAV_DIR_PATH + x for x in os.listdir(WAV_DIR_PATH) if x[-4:] == '.wav']
    rates = []
    for file in files:
        rate, values = wavfile.read(file)
        print(file)
        if(type(values[0]) == type(np.array(0))):
            values = [x[0] for x in values]
        rec_len = len(values) / rate
        part_count = int(rec_len) + 1
        parts = [values[i * rate : (i + 1) * rate] for i in range(part_count)]
        x_values = [x / rate for x in range(len(values))]
        # x_values = [i for i in range(300)]
        fourier = abs(np.fft.fft(values)) / rate
        # fft_product = [x for x in fourier]
        # for i in range(2, HPS_TIMES):
        #     tmp_fourier = copy(fourier[::i])
        #     fft_product = fft_product[:len(tmp_fourier)]
        #     fft_product *= tmp_fourier
        fft_result_sum = [0 for x in fourier]
        for part in parts:
            window = np.hamming(len(part))
            part = part * window
            if len(part) == 0:
                continue
            fourier = abs(np.fft.fft(part)) / rate
            fft_sum = [x for x in fourier]
            for i in range(2, HPS_TIMES):
                tmp_fourier = fourier[::i]
                for j in range(len(tmp_fourier)):
                    fft_sum[j] *= tmp_fourier[j]
            # plt.plot(fft_sum)
            # plt.xscale('log')
            # plt.ylim(min(fft_sum), max(fft_sum))
            # plt.show()
            for i in range(len(fft_sum)):
                fft_result_sum[i] += fft_sum[i]
        # plt.plot(fft_result_sum)
        # plt.xscale('log')
        # plt.title("Wynik")
        # plt.ylim(min(fft_result_sum), max(fft_result_sum))
        # plt.show()
        # plt.clf()
        male_sum = sum(fft_result_sum[MALE_FREQ[0]:MALE_FREQ[1]])
        female_sum = sum(fft_result_sum[FEMALE_FREQ[0]:FEMALE_FREQ[1]])
        result = None
        if(male_sum > female_sum):
            result = 'M'
        else:
            result = 'K'
        print(result)
        if result == 'M' and result == file[-5]:
            correct_males += 1
        elif result == 'M' and result != file[-5]:
            incorrect_males +=1
        elif result == 'K' and result == file[-5]:
            correct_females += 1
        elif result == 'K' and result != file[-5]:
            incorrect_females += 1
    print("Correct males:", correct_males)
    print("Incorrect males:", incorrect_males)
    print("Correct females:", correct_females)
    print("Incorrect females:", incorrect_females)
    print("Accuracy:", (correct_males + correct_females) / (correct_males + correct_females + incorrect_males + incorrect_females))